<?php
session_start();

class Connexion
{
  private $_bdd = null;
  private static $_instance = null;

  //appelée par new
  private function __construct()
  {
    $this->_bdd = new PDO('mysql:host=localhost; dbname=pendu; charset=utf8', 'root', 'root');
    $this->_bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  //appelée par clone
  private function __clone()
  {
  }

  //appelée par unserialize
  private function __wakeup()
  {
  }

  public static function getInstance()
  {
    if (is_null(self::$_instance))
      self::$_instance = new Connexion();
    return self::$_instance;
  }

  public function getBdd()
  {
    return $this->_bdd;
  }
}
