<?php require_once('header.php'); ?>

<div style="height: 100%; display: flex; flex-direction:column; align-items:center; justify-content:center">
  <div>Rentrer le pseudo de deux joueurs ci-dessous :</div>
  <form action="/def_mot.php" style="display: flex;
    flex-direction: column;
    gap: 10px;
    margin-top: 20px;">
    <input type="text" name="joueur1" placeholder="Joueur 1" required>
    <input type="text" name="joueur2" placeholder="Joueur 2" required>
    <input type="submit" value="Commencer à jouer !" name="submit">
  </form>
</div>

<?php require_once('footer.php'); ?>