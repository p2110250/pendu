<?php require_once('header.php');
if ($_SESSION['nbCoupsFait'] == $_SESSION['nbCoups']) {
  $fin = 'perdu';
  $dansLaBD = 1;
} else {
  $fin = 'gagné';
  $dansLaBD = 2;
}
?>
<div style="display: flex; flex-direction:column; align-items:center">
  <div style="    display: flex;
    justify-content: center;"><?= $_SESSION['joueur2'] ?> tu as <?= $fin ?>.
  </div>
  <div>
    <button><a href="/index.php">Revenir au début</a></button>
  </div>
</div>
<?php
$database = Connexion::getInstance()->getBdd();
$query = $database->prepare('INSERT INTO `partie` (`nom_joueur1`, `nom_joueur2`, `mot`, `victoire`, `nb_coup`) VALUES (?, ?, ?, ?, ?);');
$query->execute(array($_SESSION['joueur1'], $_SESSION['joueur2'], $_SESSION['mot'], $dansLaBD, $_SESSION['nbCoupsFait']));
$result = $query->fetch();

// unset all session variables
session_unset();


require_once('footer.php'); ?>