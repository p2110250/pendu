<?php

require_once('header.php');

?>
<div style="display: flex;
    justify-content: space-evenly;
    align-items: center; flex-direction: column; height:100%;">
  <div>
    <div style="    font-size: 25px;
    font-weight: 600;">Voici la liste des parties jouées</div>
    <?php
    $database = Connexion::getInstance()->getBdd();
    $query = $database->prepare('SELECT * FROM partie');
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    if ($result) {
      foreach ($result as $value) {

    ?>
        <div>
          <p><?= $value['nom_joueur1'] ?> à fait deviner le mot : <?= $value['mot'] ?> à <?= $value['nom_joueur2'] ?> et celui ci à <?php if ($value['victoire'] == 2) {
                                                                                                                                      echo 'gagné';
                                                                                                                                    } else {
                                                                                                                                      echo 'perdu';
                                                                                                                                    } ?> en <?= $value['nb_coup'] ?> coups.</p>
        </div>

      <?php
      }
    } else {
      ?>
      <div>Aucune partie n'a été jouée</div>
    <?php
    }
    ?>
  </div>
  <button><a href="/joueurs.php">Commencer à jouer !</a></button>
</div>

<?php require_once('footer.php');
