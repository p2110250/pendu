<?php require_once('header.php');
if (isset($_GET['joueur1']) && isset($_GET['joueur2'])) {
  $_SESSION['joueur1'] = $_GET['joueur1'];
  $_SESSION['joueur2'] = $_GET['joueur2'];
}
?>


<div style="height: 100%; display: flex; flex-direction:column; align-items:center; justify-content:center">
  <div><?= $_SESSION['joueur1'] ?> rentre le mot à faire deviner ainsi que le nombre de coups max possible ci-dessous :</div>
  <form action="/jeu.php" style="display: flex;
    flex-direction: column;
    gap: 10px;
    margin-top: 20px;">
    <input type="text" name="mot" placeholder="Mot à faire deviner" required>
    <input type="number" name="nbCoups" placeholder="Nombre de coups max" required>
    <input type="submit" value="Valider">
  </form>
  <?php if (isset($_SESSION['erreurLexique'])) {

  ?>
    <div>Le mot n'est pas dans le dictionnaire.</div>
  <?php
    unset($_SESSION['erreurLexique']);
  }
  ?>
</div>

<?php require_once('footer.php'); ?>