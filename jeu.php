<?php require_once('header.php');
if (isset($_GET['mot'])) {
  $_SESSION['mot'] = strtoupper($_GET['mot']);
  // Verify if the word is in the database lexique
  $database = Connexion::getInstance()->getBdd();
  $query = $database->prepare('SELECT * FROM `lexique` WHERE `ortho` = ?');
  $query->execute(array(strtolower($_SESSION['mot'])));
  $result = $query->fetch();
  if ($result === false) {
    $_SESSION['erreurLexique'] = 'Le mot n\'est pas dans le lexique';
    header('Location: def_mot.php');
  }
}
if (isset($_GET['nbCoups'])) {
  $_SESSION['nbCoups'] = $_GET['nbCoups'];
  $_SESSION['nbCoupsFait'] = 0;
  $_SESSION['lettre'] = '';
}
if (isset($_GET['lettre'])) {
  $_SESSION['lettre'] = $_SESSION['lettre'] . strtoupper($_GET['lettre']);

  // Verify if the letter is in the word
  $mot = strtoupper($_SESSION['mot']);
  $mot = str_split($mot);
  $lettre = strtoupper($_GET['lettre']);
  $lettre = str_split($lettre);
  $lettre = $lettre[0];
  $lettreTrouve = false;
  foreach ($mot as $key => $value) {
    if ($value == $lettre) {
      $lettreTrouve = true;
      $_SESSION['mot'][$key] = $lettre;
    }
  }
  if ($lettreTrouve == false) {
    $_SESSION['nbCoupsFait']++;
  }
}


?>

<div style='display: flex;
    flex-direction: column;
    height: 100%;
    justify-content: start;
    align-items: center;
    gap: 10vh;'>
  <div><?= $_SESSION['joueur2'] ?> tu dois deviner le mot ci-dessous en <?= $_SESSION['nbCoups'] ?> coups</div>
  <div style="display: flex; justify-content: center; align-items: center; gap: 10px;">
    <form action="/jeu.php" style="display: flex;
      flex-direction: column;
      gap: 10px;
      margin-top: 20px;">
      <input type="text" name="lettre" placeholder="Lettre" required>
      <input type="submit" value="Valider">
    </form>
    <div style="display: flex; flex-direction: column; gap: 10px;">
      <div><?= $_SESSION['joueur2'] ?> tu es à <?= $_SESSION['nbCoupsFait'] ?>/<?= $_SESSION['nbCoups'] ?> coups</div>
    </div>
  </div>
  <div>Voici le mot à deviner :
    <?php
    $mot = strtoupper($_SESSION['mot']);
    $mot = str_split($mot);
    $mot = array_map(function ($letter) {
      if (strpos($_SESSION['lettre'], $letter) !== false) {
        return $letter;
      } else {
        return '_';
      }
    }, $mot);
    $mot = implode(' ', $mot);
    echo $mot;
    ?>
  </div>
  <div>Voici les lettres que vous avez essayé et qui ne sont pas dans le mot :
    <?php
    if (isset($_GET['lettre'])) {

      $lettre = strtoupper($_SESSION['lettre']);
      $lettre = str_split($lettre);
      $lettre = array_map(function ($letter) {
        if (strpos($_SESSION['mot'], $letter) === false) {
          return $letter;
        } else {
          return '';
        }
      }, $lettre);
      $lettre = implode(' ', $lettre);
      echo $lettre;
    }
    ?>
  </div>
</div>

<?php require_once('footer.php');
// Verifie si toutes les lettres du mot est trouvé ou si le nombre de coups est atteint
$mot = str_replace(' ', '', $mot);
if ($_SESSION['mot'] === $mot || $_SESSION['nbCoupsFait'] == $_SESSION['nbCoups']) {
  header('Location: final.php');
} ?>